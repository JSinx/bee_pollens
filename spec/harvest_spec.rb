# coding: utf-8
require 'rspec'
require_relative '../harvest'

describe HarvestList do
  let(:data) do
    [{bee_id: 1, day: '2013-04-01', pollen_id: 1, miligrams_harvested: 5},
     {bee_id: 1, day: '2013-04-01', pollen_id: 2, miligrams_harvested: 7},
     {bee_id: 1, day: '2013-04-03', pollen_id: 2, miligrams_harvested: 3},
     {bee_id: 2, day: '2013-04-01', pollen_id: 1, miligrams_harvested: 6},
     {bee_id: 2, day: '2013-04-02', pollen_id: 2, miligrams_harvested: 2},
     {bee_id: 3, day: '2013-04-02', pollen_id: 1, miligrams_harvested: 4},
     {bee_id: 3, day: '2013-04-03', pollen_id: 2, miligrams_harvested: 1}]
  end

  let(:harvest) { HarvestList.new(data)}

  let(:group_by_day) do
    {'2013-04-01' =>[
        {bee_id: 1, :day=>"2013-04-01", pollen_id: 1, :miligrams_harvested=>5},
        {bee_id: 1, day: "2013-04-01", pollen_id: 2, miligrams_harvested: 7},
        {bee_id: 2, :day=>"2013-04-01", :pollen_id=>1, :miligrams_harvested=>6}],
     '2013-04-02' =>[
         {bee_id: 2, day: "2013-04-02", pollen_id: 2, miligrams_harvested: 2},
         {bee_id: 3, day: "2013-04-02", pollen_id: 1, miligrams_harvested: 4}],
     '2013-04-03' =>[
         {bee_id: 1, :day=>"2013-04-03", :pollen_id=>2, :miligrams_harvested=>3},
         {bee_id: 3, :day=>"2013-04-03", :pollen_id=>2, :miligrams_harvested=>1}]
    }
  end

  let(:sum_by_pollen) { {1=>15.0, 2=>13.0} }

  describe '.group' do
    it { expect(harvest.group(:day)).to eq group_by_day }
  end


  describe '.sum_by_pollen' do
    it { expect(harvest.sum_by_pollen).to eq sum_by_pollen }
  end
end