require 'rspec'
require_relative '../report'

describe Report do
  let(:harvest_data) do
    [{bee_id: 1, day: '2013-04-01', pollen_id: 1, miligrams_harvested: 5},
     {bee_id: 1, day: '2013-04-01', pollen_id: 2, miligrams_harvested: 7},
     {bee_id: 1, day: '2013-04-03', pollen_id: 2, miligrams_harvested: 3},
     {bee_id: 2, day: '2013-04-01', pollen_id: 1, miligrams_harvested: 6},
     {bee_id: 2, day: '2013-04-02', pollen_id: 2, miligrams_harvested: 2},
     {bee_id: 3, day: '2013-04-02', pollen_id: 1, miligrams_harvested: 4},
     {bee_id: 3, day: '2013-04-03', pollen_id: 2, miligrams_harvested: 1}]
  end

  let(:pollen_data) do
    [
        1, {name: 'Canola', sugar_per_mg: 10},
        2, {name: 'Bluebell', sugar_per_mg: 3}
    ]
  end

  let(:report) { Report.new }

  let(:pollen_rating) { [{'Canola' =>15.0}, {'Bluebell' =>13.0}] }
  let(:sugar_rating) {  [{'Canola' =>150.0}, {'Bluebell' =>39.0}] }

  before do
    allow_any_instance_of(HarvestList).to receive(:load_from_file).and_return(harvest_data)

    report.instance_variable_set(:@harvest,  HarvestList.new(harvest_data) )
    report.instance_variable_set(:@pollen,  Pollen[*pollen_data] )
  end

  context 'public methods' do

    let(:sugar_by_day) { [{'2013-04-01' =>131.0}, {'2013-04-02' =>46.0}, {'2013-04-03' =>12.0}] }
    let(:sugar_by_bee) { [{1=>80.0}, {2=>66.0}, {3=>43.0}] }

    describe '.pollen_rating' do
      it { expect(report.pollen_rating).to eq pollen_rating }
    end

    describe '.sugar_rating' do
      it { expect(report.sugar_rating).to eq sugar_rating }
    end

    describe '.sugar_by_day' do
      it { expect(report.sugar_by_day).to eq sugar_by_day }
    end

    describe '.sugar_by_bee' do
      it { expect(report.sugar_by_bee).to eq sugar_by_bee }
    end
  end

  context 'private methods' do
    describe '.pollen_to_sugar' do
      let(:pollen_harvest) { {1=>15.0, 2=>13.0} }
      let(:pollen_sugar_harvest) { {1=>150.0, 2=>39.0} }

      it { expect(report.send(:pollen_to_sugar, pollen_harvest)).to eq pollen_sugar_harvest }
    end

    describe '.sort_and_name' do
      let(:unsorted_data) { {1=>11.0, 2=>13.0} }
      let(:sorted_data) {  [{'Bluebell' =>13.0}, {'Canola' =>11.0}] }

      it { expect(report.send(:sort_and_name, unsorted_data)).to eq sorted_data }
    end
  end
end