require 'rspec'
require_relative '../pollen'

describe Pollen do
  let(:data) do
    [
      1, {name: 'Canola', sugar_per_mg: 10},
      2, {name: 'Bluebell', sugar_per_mg: 3}
    ]
  end

  let(:pollen) { Pollen[*data] }

  it 'should do something' do
    expect(pollen.get_sugar(1)).to eq 10
    expect(pollen.get_name(1)).to eq 'Canola'
  end

  it 'should do something' do
    expect(pollen.get_sugar(0)).to eq nil
  end
end