# coding: utf-8
class Table
  CELL_LENGTH = 15

  def line(length)
    puts '-'*length
  end

  def row(row_data)
    str = row_data.map { |item| item.to_s.center(CELL_LENGTH) }.join('|')
    p "|#{str}|"
  end

  def header_row(row_data)
    line(char_numbers(row_data))
    row(row_data)
    line(char_numbers(row_data))
  end

  def table(name, header_data, data)
    row_length = char_numbers(header_data)

    puts "\n Таблица: #{name}"
    header_row(header_data)
    data.each do |item|
      row(item.to_a.flatten)
    end
    line(row_length)
  end

  def char_numbers(row_data)
    (CELL_LENGTH + 1) * row_data.size + 2
  end
end