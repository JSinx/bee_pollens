# coding: utf-8
require_relative 'report'

class Main
  MAIN_MENU = ['Выберите отчет:',
       '1 - Какая пыльца была наиболее популярна среди пчел',
       '2 - Из пыльцы какого типа было получено больше всего сахара',
       '3 - Сколько добыто сахара по дням',
       '4 - Эффективность пчел',
       '0 - Выход'
      ]

  ERROR_SELECT = 'Неверный выбор. Попробуйте снова.'

  def initialize
    @report = Report.new('harvest.csv', 'pollens.csv')
    @table = Table.new
  end

  def main_menu
    loop do
      MAIN_MENU.each { |item| puts item }
      select_item = gets.to_i
      puts select_item
      if (select_item >= 1) && (select_item <= 4)
        show_report(select_item.to_i)
      elsif select_item == 0
        return
      else
        puts ERROR_SELECT
      end
    end
  end

  def show_report(report_idx)
    case report_idx
      when 1
        @table.table('Рейтинг пыльцы', ['Пыльца','Кол-во пыльцы'], @report.pollen_rating )
      when 2
        @table.table('Рейтинг сахара', ['Пыльца','Кол-во сахара'], @report.sugar_rating )
      when 3
        @table.table('Рейтинг сахара по дням', ['День','Кол-во сахара'], @report.sugar_by_day )
      when 4
        @table.table('Рейтинг сахара по пчелам', ['Пчела','Кол-во сахара'], @report.sugar_by_bee )
      end
  end
end

Main.new.main_menu
