require 'csv'
require_relative 'pollen'

class HarvestList < Array
  attr_reader :list, :pollen

  def group_and_sum(group_key, sum_key)
    result = Hash.new(0)

    group(group_key).each do |key, value|
      result[key] = value.inject(0) {|sum, hash| sum + hash[sum_key]}.round(2)
    end
    result
  end

  def load_from_file(file_name)
    CSV::HeaderConverters[:strip] = lambda { |s| s.strip.to_sym rescue s }
    CSV.foreach(file_name, :headers => true, :header_converters => :strip, :converters => :all) do |row|
      h = Hash[row.headers.zip(row.fields)]
      self << h
    end
    self
  end

  def group(key)
    group_by { |h| h[key] }
  end

  def sum_by_pollen
    group_and_sum(:pollen_id, :miligrams_harvested)
  end
end