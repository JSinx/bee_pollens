# coding: utf-8
require_relative 'harvest'
require_relative 'table'

class Report
  def initialize(harvest_file = nil, pollen_file = nil)
    @harvest = HarvestList.new.load_from_file(harvest_file)
    @pollen = Pollen.new(pollen_file)
  end

  def pollen_rating
    result = @harvest.sum_by_pollen
    sort_and_name(result)
  end

  def sugar_rating
    pollen_harv = @harvest.sum_by_pollen
    result = pollen_to_sugar(pollen_harv)
    sort_and_name(result)
  end

  def sugar_by_day
    by_days = @harvest.group(:day)
    sum_sub_group(by_days)
  end

  def sugar_by_bee
    by_bee = @harvest.group(:bee_id)
    sum_sub_group(by_bee)
  end

  private

  def pollen_to_sugar(pollen_hash)
    result = {}
    pollen_hash.each do |pk, pv|
      result[pk] = pv * @pollen.get_sugar(pk)
    end
    result
  end

  def sort_and_name(value)
    value.sort {|a1,a2| a2[1]<=>a1[1]}.map { |v| Hash[*[@pollen.get_name(v[0]), v[1]]] }
  end

  def sum_sub_group(group_data)
    result = {}

    group_data.each do |date_key, value|
      har_value = HarvestList.new(value)
      pollen_in_day = har_value.sum_by_pollen

      result[date_key] = (pollen_to_sugar(pollen_in_day).inject(0) {|sum, hash| sum + hash[1] }).round(2)
    end
    result.sort {|a1,a2| a2[1]<=>a1[1]}.map { |v| Hash[*[v[0], v[1]]] }
  end
end
