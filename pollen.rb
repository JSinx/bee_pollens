require 'csv'

class Pollen < Hash
  def initialize(file_name)
    load_from_file(file_name) if file_name
  end

  def get_name(id)
    self[id][:name] if self[id]
  end

  def get_sugar(id)
    self[id][:sugar_per_mg] if self[id]
  end

  private

  def load_from_file(file_name)
    CSV.foreach(file_name, :headers => true, :header_converters => :symbol, :converters => :all) do |row|
      self[row[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
    end
  end
end

